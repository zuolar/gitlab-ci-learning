# GitLab-CI-Learning

## 測試涵蓋率

```shell
go test -cover
```

## 測試效能

```shell
go test -benchmem -bench .
```
