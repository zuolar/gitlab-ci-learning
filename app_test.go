package main

import (
	"testing"
)

func TestMain(t *testing.T) {
	main()
}

func TestGetAppName(t *testing.T) {
	app := GetAppName()
	if app != "golang" {
		t.Error("應用名稱錯誤 : ", app)
		return
	}
	t.Log("應用名稱正確 : ", app)
}

func BenchmarkGetAppName(b *testing.B) {
	GetAppName()
}
